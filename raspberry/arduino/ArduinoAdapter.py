import abc


class ArduinoAdapter(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def sendMessage(self, message):
        return
