import time
import serial

from arduino.ArduinoAdapter import ArduinoAdapter
from arduino.ArduinoError import ArduinoError


class ArduinoSerialAdapter(ArduinoAdapter):
    def __init__(self, port):
        self.port = port

    def sendMessage(self, message):
        sentBytes = self._sendMessageWithSerial(message)
        if sentBytes <= 0:
            raise ArduinoError("No bytes of message:\"{}\" were sent to Arduino".format(message))

    def _sendMessageWithSerial(self, message):
        try:
            with serial.Serial(self.port) as ser:
                time.sleep(8)
                return ser.write(message.encode('utf-8'))
        except Exception as e:
            raise ArduinoError("Could not send message:\"{}\" to Arduino due to an underlying error"
                               .format(message)) from e