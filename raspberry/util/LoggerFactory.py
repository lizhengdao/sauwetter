import logging

import sys


class LoggerFactory(object):

    @staticmethod
    def getLogger(loggerName):
        logger = logging.getLogger(loggerName)
        logger.setLevel(logging.INFO)

        logger.addHandler(LoggerFactory.__createConsoleHandler())
        logger.addHandler(LoggerFactory.__createFileHandler())
        return logger

    @staticmethod
    def __createConsoleHandler():
        consoleHandler = logging.StreamHandler(sys.stdout)
        consoleHandler.setFormatter(LoggerFactory.__createFormatter())
        return consoleHandler

    @staticmethod
    def __createFileHandler():
        fileHandler = logging.FileHandler("Sauwetter.log")
        fileHandler.setFormatter(LoggerFactory.__createFormatter())
        return fileHandler

    @staticmethod
    def __createFormatter():
        formatString = "[%(levelname)s] %(asctime)s - %(pathname)s - %(message)s"
        dateString = "%Y-%m-%d %H:%M:%S"
        return logging.Formatter(formatString, dateString)