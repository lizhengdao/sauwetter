import unittest
import os

from weather.WeatherProviderFactory import WeatherProviderFactory
from weather.Forecast import Forecast


class OpenWeatherMapAdapterIT_getForecast(unittest.TestCase):
    def test_WhenSetupValid_ReturnsForecast(self):
        # Given
        with open(self.__getApiKey(), "r") as f:
            owmToken = f.read().strip()
        os.environ[WeatherProviderFactory.owmTokenEnvironmentVariableName] = \
            owmToken
        owmAdapter = WeatherProviderFactory.createWeatherProvider()
        # When
        actual = owmAdapter.getCurrentForecast()
        # Then
        self.assertIsInstance(actual, Forecast)
        print(actual.__dict__)

    def __getApiKey(self):
        testDirectory = os.path.dirname(__file__)
        return os.path.join(testDirectory, os.pardir, os.path.relpath("resources/OpenWeatherMapApiKey"))
