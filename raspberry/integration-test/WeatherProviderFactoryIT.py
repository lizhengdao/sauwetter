import unittest
import os

from weather.OpenWeatherMapAdapter import OpenWeatherMapAdapter
from weather.WeatherProviderFactory import WeatherProviderFactory


class WeatherProviderIT_createWeatherProvider(unittest.TestCase):
    def test_WhenEnvironmentVariableNotSet_ThrowsError(self):
        # Then
        self.assertRaises(Exception,
                          WeatherProviderFactory.createWeatherProvider)

    def test_WhenEnvironmentVariableSet_ReturnsOpenWeatherMapAdapter(self):
        # Given
        any_token = "123"
        os.environ[WeatherProviderFactory.owmTokenEnvironmentVariableName] = \
            any_token
        # When
        weatherProvider = WeatherProviderFactory.createWeatherProvider()
        # Then
        self.assertIsInstance(weatherProvider, OpenWeatherMapAdapter)
