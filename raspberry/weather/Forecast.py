class Forecast(object):
    def __init__(self, tempMax=None, tempMin=None, tempAvg=None,
                 dateTimeStart=None, dateTimeEnd=None, condition=None):
        self.temperatureMax = tempMax
        self.temperatureMin = tempMin
        self.temperatureAvg = tempAvg
        self.dateTimeStart = dateTimeStart
        self.dateTimeEnd = dateTimeEnd
        self.condition = condition
