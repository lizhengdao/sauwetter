import abc


class WeatherProvider(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def getCurrentForecast(self):
        return

    @abc.abstractmethod
    def getNextForecast(self):
        return

    @abc.abstractmethod
    def getCurrentWeather(self):
        return