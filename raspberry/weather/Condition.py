from enum import Enum


class Condition(Enum):
    THUNDERSTORM = 1
    RAIN = 2
    SNOW = 3
    FOG = 4
    CLEAR = 5
    CLOUDS = 6
    UNDEFINED = 7
