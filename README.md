# sauwetter

Goal of this project is emulating the public weather station in Aachen, which uses light indicators to encode the weather forecast. The design is called [Aachener Wettersäule](https://de.wikipedia.org/wiki/Aachener_Wetters%C3%A4ule).

## How it works
The project uses two parts to emulate the *Wettersäule*.  
First, an Arduino emulates the physical setup. Second, a Python application, which for example can be run on a Raspberry Pi, handles fetching current weather data from an API, translating and sending the data to Arduino.

### Modelling the station
The light indicators are emulated using an [Arduino](https://gitlab.com/bauerei/sauwetter/-/wikis/breadboard-circuit), which uses LEDs to recreate the physical model. How the LEDs light up, can be controlled using the serial interface of Arduino. Values sent to Arduino must follow the defined [protocol](https://gitlab.com/bauerei/sauwetter/-/wikis/data-exchange-protocol).

### Fetching weather data
This project uses an API provided by [openweathermap.org](https://openweathermap.org/) to fetch current weather data.  
It uses the [3 hour forecast data](https://openweathermap.org/forecast5#5days). The returned respone contains [condition codes](https://openweathermap.org/weather-conditions#Weather-Condition-Codes-2), which must be mapped to the supported conditions as described in the [protocol](https://gitlab.com/bauerei/sauwetter/-/wikis/data-exchange-protocol). 

## License
[MIT](https://choosealicense.com/licenses/mit/)