/**
 * -------------------------------
 * Pin configuration
 * -------------------------------
 */
const byte LATCH_PIN = 4;     // White -> (12) ST_CP on 74HC595
const byte CLOCK_PIN = 2;     // Orange -> (11) SH_CP on 74HC595
const byte DATA_PIN = 5;      // Blue -> (14) DS on 74HC595
const byte OE_PIN = 3;        // Yellow -> (13) OE on 74HC595

const byte RED_PIN = 11;
const byte GREEN_PIN = 10;
const byte BLUE_PIN = 9;

/**
 * -------------------------------
 * Temperature bar configuration
 * -------------------------------
 */
//Size is given in number of LEDs
const byte temperatureBarSize = 6;
//Current wiring controls Q2-Q7 of 74HC595, i.e. the 6 highest bits (out of 8 bits)
const short temperatureValues[temperatureBarSize + 1] = {0, 128, 192, 224, 240, 248, 252};
byte temperatureBarIntensity = 240;

/**
 * -------------------------------
 * Condition sphere configuration
 * -------------------------------
 */
byte conditionSphereIntensity = 10;
//Set how many times the conditions LED blinks, before the temperature LED is changed
byte conditionsFrequency = 2;

/**
 * -------------------------------
 * Temp variables
 * -------------------------------
 */
byte currentTemperatureValueIndex = 0;
byte conditionSphereOn = 1;

int loopDelayInMilliseconds = 1000;

typedef void (*TemperatureFunction)(void);
typedef void (*ConditionFunction)(void);

struct Config {
  TemperatureFunction temperatureFunction;
  ConditionFunction conditionFunction;
  byte constantCondition;
}currentConfig;

void initConfig() {
  currentConfig.temperatureFunction = temperatureRising;
  currentConfig.conditionFunction = conditionInitial;
  currentConfig.constantCondition = 0;
}

void setup() {
  initConfig();
  
  pinMode(LATCH_PIN, OUTPUT);
  pinMode(DATA_PIN, OUTPUT);  
  pinMode(CLOCK_PIN, OUTPUT);
  pinMode(OE_PIN, OUTPUT);
  analogWrite(OE_PIN, temperatureBarIntensity);

  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);  
  pinMode(BLUE_PIN, OUTPUT);

  Serial.begin(9600);
}

void loop() {
  /**
   * -------------------------------
   * Read current forecast from serial
   * -------------------------------
   */
  if (Serial.available()) {
    byte serialInput[3];
    readConfigFromSerial(serialInput);
    updateConfig(serialInput);
  }
  
  for(int i=0; i <= conditionsFrequency; i++) {
    swapCondition(currentConfig.constantCondition, currentConfig.conditionFunction);
    
    if(i == conditionsFrequency) {
      currentConfig.temperatureFunction();
    }
    
    delay(loopDelayInMilliseconds);
  }
}

void readConfigFromSerial(byte* serialInput) {
  byte i = 0;
  while (Serial.available()) {
    char inputChar = Serial.read();
    if (inputChar != '-') {
      serialInput[i] = inputChar - '0';
      i++;
    }
  }
}

void updateConfig(byte* serialInput) {
  updateConditionFunction(serialInput[0]);
  updateConstantCondition(serialInput[1]);
  updateTemperatureFunction(serialInput[2]);
}

void updateConditionFunction(byte value) {
  if (value == 0) {
    currentConfig.conditionFunction = conditionSunny;
  } else if (value == 1) {
    currentConfig.conditionFunction = conditionCloudy;
  } else if (value == 2) {
    currentConfig.conditionFunction = conditionRainy;
  }
}

void updateConstantCondition(byte value) {
  currentConfig.constantCondition = value;
}

void updateTemperatureFunction(byte value) {
  if (value == 0) {
    currentConfig.temperatureFunction = temperatureRising;
  } else if (value == 1) {
    currentConfig.temperatureFunction = temperatureFalling;
  } else if (value == 2) {
    currentConfig.temperatureFunction = temperatureConstant;
  }
}

void swapCondition(byte constantCondition, void (*conditionFunction)()) {
  if (!constantCondition) {
    if(conditionSphereOn) {
      conditionOff();
      conditionSphereOn = 0;
    }
    else {
      conditionFunction();
      conditionSphereOn = 1;
    }
  }
  else {
    conditionFunction();
  }
}

/**
 * -------------------------------
 * Possible condition sphere configurations
 * -------------------------------
 */
void conditionSunny() {
  analogWrite(BLUE_PIN, conditionSphereIntensity);
}

void conditionCloudy() {
  analogWrite(RED_PIN, conditionSphereIntensity);
  analogWrite(GREEN_PIN, conditionSphereIntensity / 2);
}

void conditionRainy() {
  analogWrite(RED_PIN, conditionSphereIntensity);
  analogWrite(GREEN_PIN, conditionSphereIntensity);
  analogWrite(BLUE_PIN, conditionSphereIntensity);
}

void conditionOff() {
  analogWrite(RED_PIN, 0);
  analogWrite(GREEN_PIN, 0);
  analogWrite(BLUE_PIN, 0);
}

void conditionInitial() {
  analogWrite(RED_PIN, conditionSphereIntensity);
  analogWrite(GREEN_PIN, 0);
  analogWrite(BLUE_PIN, conditionSphereIntensity);
}

/**
 * -------------------------------
 * Possible temperature bar configurations
 * -------------------------------
 */
void temperatureRising() {
  updateShiftRegister(temperatureValues[currentTemperatureValueIndex]);
  currentTemperatureValueIndex = (currentTemperatureValueIndex + 1) % (temperatureBarSize + 1);
}

void temperatureFalling() {
  updateShiftRegister(temperatureValues[currentTemperatureValueIndex]);
  if(currentTemperatureValueIndex - 1 >= 0) {
    currentTemperatureValueIndex--;
  }
  else {
    currentTemperatureValueIndex = temperatureBarSize;
  }
}

void temperatureConstant() {
  updateShiftRegister(temperatureValues[temperatureBarSize]);
}

/**
 * -------------------------------
 * Helper functions
 * -------------------------------
 */
void updateShiftRegister(byte value) {
   digitalWrite(LATCH_PIN, LOW);
   shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, value);
   digitalWrite(LATCH_PIN, HIGH);
}
